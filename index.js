const fs = require('fs')
const util = require('util')
const moment = require('moment')
const path = require('path');
const appDir = path.dirname(require.main.filename);

const logFile = fs.createWriteStream(appDir+'/debug.log')
const logStdOut = process.stdout

function logger(message, level = 'INFO') { 
	var log = moment().format('YYYY/MM/DD HH:mm:ss SSS')+' ['+level+'] '+util.format(message)+'\n';
	logFile.write(log)
	logStdOut.write(log)
}

module.exports = logger